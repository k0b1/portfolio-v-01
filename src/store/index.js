export default {
  state: {
    activeBuffers: ["README.md"],
    activateBuffer: "README.md",
    activateProjectDescription: "",
    filterProjectsBasedOnStatusbar: "all"
  },
  getters: {
    getActiveBuffers(state) {
      return state.activeBuffers;
    },
    getActivateBuffer(state) {
      return state.activateBuffer;
    },
    getActivatedProjectDescription(state) {
      return state.activateProjectDescription;
    },
    getFilteredProjectsBasedOnSidebar(state) {
      return state.filterProjectsBasedOnStatusbar;
    }
  },
  mutations: {
    activeBuffersIsChanged(state, value) {
      state.activeBuffers.push(value);
    },
    thisBufferIsActivated(state, value) {
      state.activateBuffer = value;
    },
    projectDescriptionIsActivated(state, value) {
      state.activateProjectDescription = value;
    },
    filteredProjectsBasedOnSidebar(state, value) {
      state.filterProjectsBasedOnStatusbar = value;
    }
  },
  actions: {
    changeActiveBuffer({ commit }, value) {
      commit("activeBuffersIsChanged", value);
    },
    activateThisBuffer({ commit }, value) {
      commit("thisBufferIsActivated", value);
    },
    activateProjectDescription({ commit }, value) {
      commit("projectDescriptionIsActivated", value);
    },
    activateFilteringBasedOnStatusbar({ commit }, value) {
      commit("filteredProjectsBasedOnSidebar", value);
    }
  }
};
