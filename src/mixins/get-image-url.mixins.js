export default {
  methods: {
    getImageUrl: (image, path) => {
      console.log(image, path);
      let images = require.context(path, false).default;
      return images("./" + image);
    }
  }
};
