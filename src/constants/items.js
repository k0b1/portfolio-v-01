const Items = {
  aboutMe: "About Me",
  personal: "personal.vue",
  preferedTools: "prefered-technologies.vue",
  jobs: "jobs-I-am-interested-in.vue",
  myWork: "Work History",
  myWorkFile: "work-history.vue",
  myProjects: "My projects",
  myProjectsFile: "my-projects.vue",
  killSomeBugs: "Kill some bugs?",
  killSomeBugsFile: "game.js",
  contact: "Contact",
  contactFile: "contact.php",
  file: "file",
  edit: "edit",
  options: "options",
  buffers: "buffers",
  readme: "README.md"
};

const myWork = [
  {
    position: "Fullstack developer",
    company: "Noah",
    involvement: "part time, remote",
    duration: "Jun 2017 - present",
    location: "Denmark",
    link: "https://noah-iam.com/en/",
    technologies:
      "PHP, JS, jQuery, SQL, HTML, SCSS, CSS, Redmine, REST API, Stripe",
    description: [
      " Description: In the highly developed market of assets management tools a small company in Denmark is trying to cut costs and make the software more accessible to managers and other user types than IT technicians.",
      " Project is a side project of the company, few people work on it part-time but for last 5+ years it developed a lot. My role is threefold - one is to fix bugs which occur here and there, other is to add new functionalities to the existing ones, as customers ask to. And the third, most exciting part is developing of new fetures, such as email reports for user tickets, chat/feed section, etc."
    ]
  },
  {
    position: "In-house web developer",
    company: "Widewalls",
    involvement: "full time, remote",
    duration: "November 2017 - August, 2020",
    location: "Belgrade, Serbia",
    link: "https://widewalls.ch/",
    technologies:
      "PHP, JS, jQuery, Wordpress, Vue, SQL, HTML,SCSS, CSS, Agile methodology, Jira, Bitbucket, Github, Slack",
    description: [
        "Established in 2013, valued for the quality of its content and usability, Widewalls is amongst the fastest growing and most successful modern and contemporary art online resources in the world.","For me the biggest challenge with working on such a huge web site was how to find a proper way through the years of older, organic build, code. Daily tasks include everything from fixing bugs, changing plugins features, building new features or working on website SEO improvements. I was also part of the website rewrite, which started in November of 2019, and lasted for 9 months. My position in rewriting was mostly styling of Vue components with SCSS."
    ]
  },
  {
    position: "Freelancer",
    company: "Upwork",
    involvement: "part time, remote",
    duration: "Januar 2016 - Jun 2017 ",
    location: "Banja Luka, Bosnia and Herzegovina",
    link: "https://www.upwork.com/o/profiles/users/~01bd5b9aab2aa90021/",
    technologies: "PHP, Wordpress, HTML, CSS",
    description: [
      "During this period I had a few freelancer type of jobs, partly as data entry specialist ( searching for contact information of different art galleries, or building the songs/artists database for Italian radio station ), and partly as junior web developer, creating my first, simple websites (such as http://pozdravizbeograda.com/ )."
    ]
  }
];

const SidebarItems = [
  {
    folder: "About Me",
    files: [
      "personal.vue",
      "prefered-technologies.vue",
      "jobs-I-am-interested-in.vue"
    ]
  },
  {
    folder: "Work History",
    files: ["work-history.vue"]
  },
  {
    folder: "My Projects",
    files: ["my-projects.vue"]
  },
  {
    folder: "Kill Some Bugs?",
    files: ["game.js"]
  },
  {
    folder: "Contact",
    files: ["contact.php"]
  },
  {
    file: "README.md"
  }
];

const TopNavigationItemsContent = {
  file: ["Visit new file", "Open file", "Open directory"],
  edit: ["Undo", "Redo", "Cut", "Copy", "Paste"],
  options: ["Highlight active region", "Highlight Matching Paranthesis"],
  buffers: ["About me", "My work", "My projects", "Kill some bugs?", "Contact"]
};

const Programming = {
  languages: [
    {
      name: "Javascript",
      icon: "iconfinder_js_282802.svg"
    },
    {
      name: "jQuery",
      icon: "iconfinder_JQuery_logo_282806.svg"
    },
    {
      name: "Vue",
      icon: "iconfinder_vue-dot-js_4691448.svg"
    },
    {
      name: "Angular",
      icon: "angular.svg"
    },
    {
      name: "PHP",
      icon: "iconfinder_256_Php_4518869.svg"
    },
    {
      name: "Wordpress",
      icon: "iconfinder_27_940975.svg"
    },
    {
      name: "CSS/SCSS",
      icon: "iconfinder_288_Sass_logo_4375066.svg"
    },
    {
      name: "Git",
      icon: "iconfinder_141_Git_logo_logos_4373153.svg"
    }
  ]
};

const SocialNetworks = [
  {
    name: "Upwork",
    icon: "iconfinder_79-upwork_1929178.svg",
    link: "https://www.upwork.com/o/profiles/users/~01bd5b9aab2aa90021/"
  },
  {
    name: "Linkedin",
    icon: "iconfinder_linkedin_294671.svg",
    link: "https://www.linkedin.com/in/rastislav-kobac-020460131/"
  },
  {
    name: "Gitlab",
    icon: "iconfinder_144_Gitlab_logo_logos_4373151.svg",
    link: "https://gitlab.com/k0b1"
  }
];

export {
  Items,
  SidebarItems,
  TopNavigationItemsContent,
  Programming,
  SocialNetworks,
  myWork
};
