import Vue from "vue";
import Vuex from "vuex";
import App from "./App.vue";
import storeData from "./store/index";
import vuetify from "./plugins/vuetify";
import Vuetify from "vuetify";
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(Vuetify);
Vue.use(Vuex, VueAxios, axios);

const store = new Vuex.Store(storeData);

Vue.config.productionTip = false;

// custom directive
Vue.directive("click-outside", {
  bind: function(el, binding, vnode) {
    el.clickOutsideEvent = function(event) {
      if (!(el == event.target || el.contains(event.target))) {
        vnode.context[binding.expression](event);
      }
    };
    document.body.addEventListener("click", el.clickOutsideEvent);
  },
  unbind: function(el) {
    document.body.removeEventListener("click", el.clickOutsideEvent);
  }
});

new Vue({
  render: h => h(App),
  vuetify,
  store: store
}).$mount("#app");
