class PortfolioGame {
  constructor(context, canvas, pacmanLife) {
    this.context = context;
    this.canvas = canvas;

    this.secondsPassed = 0;
    this.oldTimeStamp = 0;
    this.movingSpeed = 50;
    this.direction = "";
    this.gameObjects = [];
    this.pacman = "";
    this.weapon = "";
    this.createWorld();
    this.pacmanLife = pacmanLife;
    this.enemyLife = 20;

    this.isColliding = false;
    this.frameID = "";
    this.attack = false;
    this.rand = 0;
    this.selectWeapon = ["vue-small.png", "php-small.png", "angular.png"];
  }

  update(secondsPassed) {
    //    if (!isNaN(secondsPassed)) {
    this.x += this.movingSpeed * secondsPassed;
    this.y += this.movingSpeed * secondsPassed;
    //    }
  }
  clear() {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }
  listenPacman() {
    const arrowsEvent = event => {
      if (event.keyCode === 39 || event.charCode === 39) {
        this.direction = "right";
      } else if (event.keyCode === 37) {
        this.direction = "left";
      } else if (event.keyCode === 38) {
        this.direction = "up";
      } else if (event.keyCode === 40) {
        this.direction = "bottom";
      }

      if (event.keyCode === 65) {
        // attack
        if (this.pacman) {
          this.pacman.attack(
            this.weapon,
            this.direction,
            this.pacman.x,
            this.pacman.y,
            this.selectWeapon,
            this.rand
          );
        }
      }
      this.rand = Math.floor(Math.random() * this.selectWeapon.length);
      if (this.pacmanLife === 0) {
        document.removeEventListener("keydown", arrowsEvent);
      }
    };

    document.addEventListener("keydown", arrowsEvent);
  }

  listenToResult() {
    if (this.pacmanLife <= 0 || this.enemyLife <= 0) {
      window.cancelAnimationFrame(this.frameID);
      delete this.pacman;
      for (let i = 0; i < this.gameObjects.length; i++) {
        delete this.gameObjects[i];
      }
      this.clear();
    }
    return [this.pacmanLife, this.enemyLife];
  }
  gameLoop(timeStamp) {
    // Calculate the number of seconds passed since the last frame
    this.secondsPassed = (timeStamp - this.oldTimeStamp) / 1000;
    this.oldTimeStamp = timeStamp;
    // Move forward in time with a maximum amount
    this.secondsPassed = Math.min(this.secondsPassed, 0.1);

    for (let i = 0; i < this.gameObjects.length; i++) {
      if (this.gameObjects[i]) {
        this.gameObjects[i].update(this.secondsPassed);
      }
    }

    this.detectCollisions();

    this.pacman.update(this.direction);

    if (this.gameObjects) {
      for (let i = 0; i < this.gameObjects.length; i++) {
        this.gameObjects[i].draw();
        this.detectPacmanCollision(this.gameObjects[i]);
        let registerWeaponCollision = this.detectWeaponCollision(
          this.gameObjects[i]
        );
        if (registerWeaponCollision) {
          this.gameObjects[i].x = 712;
          this.gameObjects[i].y = 512;
          this.enemyLife--;
        }
      }
    }

    this.pacman.draw(this.direction);

    let result = this.listenToResult();
    if (result[0] === 0 || result[1] === 0) {
      window.cancelAnimationFrame(this.frameID);
      return;
    }
    this.frameID = window.requestAnimationFrame(this.gameLoop.bind(this));
  }

  createWorld() {
    this.clear();
    this.gameObjects = [
      // new Enemy(this.context, 250, 50, 0, 50),
      // new Enemy(this.context, 250, 300, 0, -50),
      new Enemy(this.context, 150, 0, 50, 50),
      new Enemy(this.context, 250, 150, 50, 50),
      new Enemy(this.context, 350, 75, -50, 50),
      new Enemy(this.context, 300, 300, 50, -50),
      new Enemy(this.context, 550, 150, 50, 50),
      new Enemy(this.context, 550, 75, -50, 50),
      new Enemy(this.context, 600, 300, 50, -50)
    ];
    this.pacman = new Pacman(this.context, 0, 0, 0, 50);
    this.weapon = new Weapon(this.context, this.pacman.x, this.pacman.y, 0, 50);
  }

  detectWeaponCollision(obj1) {
    for (let i = 0; i < this.gameObjects.length; i++) {
      if (
        this.rectIntersect(
          obj1.x,
          obj1.y,
          obj1.width,
          obj1.height,
          this.weapon.blaX,
          this.weapon.blaY,
          this.weapon.width,
          this.weapon.height
        )
      ) {
        this.weapon.isColliding = true;
        this.clear();
        this.weapon.blaX = 9999;
        this.weapon.blaY = 9999;
        window.cancelAnimationFrame(this.weapon.weaponAttackID);
        return true;
      }
    }
  }
  detectPacmanCollision(obj1) {
    for (let i = 0; i < this.gameObjects.length; i++) {
      if (
        this.rectIntersect(
          obj1.x,
          obj1.y,
          obj1.width,
          obj1.height,
          this.pacman.x,
          this.pacman.y,
          this.pacman.width,
          this.pacman.height
        )
      ) {
        this.pacman.isColliding = true;
        this.clear();
        this.pacman.x = 0;
        this.pacman.y = 0;
        this.pacmanLife--;
        this.context.fillStyle = "#FF0000";
        this.context.fillRect(0, 0, 712, 512);
        setTimeout(() => {
          this.context.fillStyle = "#272822";
          this.context.fillRect(0, 0, 712, 512);
        }, 60);
        if (this.pacmanLife < 0) {
          this.pacmanLife = 0;
        }
      }
    }
  }

  detectCollisions() {
    let obj1;
    let obj2;

    // Reset collision state of all objects
    for (let i = 0; i < this.gameObjects.length; i++) {
      this.gameObjects[i].isColliding = false;
    }

    // Start checking for collisions
    for (let i = 0; i < this.gameObjects.length; i++) {
      obj1 = this.gameObjects[i];
      for (let j = i + 1; j < this.gameObjects.length; j++) {
        obj2 = this.gameObjects[j];
        // Compare object1 with object2
        let vCollision = { x: obj2.x - obj1.x, y: obj2.y - obj1.y };
        let distance = Math.sqrt(
          (obj2.x - obj1.x) * (obj2.x - obj1.x) +
            (obj2.y - obj1.y) * (obj2.y - obj1.y)
        );
        let vCollisionNorm = {
          x: vCollision.x / distance,
          y: vCollision.y / distance
        };
        let vRelativeVelocity = { x: obj1.vx - obj2.vx, y: obj1.vy - obj2.vy };
        let speed =
          vRelativeVelocity.x * vCollisionNorm.x +
          vRelativeVelocity.y * vCollisionNorm.y;
        if (
          this.rectIntersect(
            obj1.x,
            obj1.y,
            obj1.width,
            obj1.height,
            obj2.x,
            obj2.y,
            obj2.width,
            obj2.height
          )
        ) {
          obj1.isColliding = true;
          obj2.isColliding = true;
          if (speed < 0) {
            break;
          }
          obj1.vx -= speed * vCollisionNorm.x;
          obj1.vy -= speed * vCollisionNorm.y;
          obj2.vx += speed * vCollisionNorm.x;
          obj2.vy += speed * vCollisionNorm.y;
        }
      }
    }
  }

  rectIntersect(x1, y1, w1, h1, x2, y2, w2, h2) {
    if (x2 > w1 + x1 || x1 > w2 + x2 || y2 > h1 + y1 || y1 > h2 + y2) {
      return false;
    }
    return true;
  }
}

class GameObject {
  constructor(context, x, y, vx, vy) {
    this.context = context;
    this.x = x;
    this.y = y;
    this.vx = vx;
    this.vy = vy;
    this.width = 42;
    this.height = 42;
  }

  clear(x, y, width, height) {
    this.context.clearRect(x, y, width, height);
  }
}

class Pacman extends GameObject {
  constructor(context, x, y, vx, vy) {
    super(context, x, y, vx, vy);
  }

  draw(direction) {
    let mySelf = new Image();
    mySelf.src = "sprites/RunShoot1.png";
    mySelf.src = "pac.png";

    mySelf.onload = () => {
      this.context.imageSmoothingEnabled = false;
      this.clear(this.x - 10, this.y - 10, 58, 58);
      this.context.drawImage(mySelf, 320, 0, 32, 32, this.x, this.y, 32, 32);

      if (direction == "right") {
        this.context.drawImage(mySelf, 320, 0, 32, 32, this.x, this.y, 32, 32);
      } else if (direction == "left") {
        this.context.drawImage(mySelf, 320, 64, 32, 32, this.x, this.y, 32, 32);
      } else if (direction == "up") {
        this.context.drawImage(mySelf, 320, 96, 32, 32, this.x, this.y, 32, 32);
      } else if (direction == "bottom") {
        this.context.drawImage(mySelf, 320, 32, 32, 32, this.x, this.y, 32, 32);
      }
    };
  }

  update(directions) {
    if (directions == "right") {
      this.x += 4;
    } else if (directions == "left") {
      this.x -= 4;
    } else if (directions == "up") {
      this.y -= 4;
    } else if (directions == "bottom") {
      this.y += 4;
    }

    if (this.x > 778) this.x = 0;
    if (this.x < -64) this.x = 778;
    if (this.y > 566) this.y = 0;
    if (this.y < -64) this.y = 566;
  }

  attack(weaponObj, direction, x, y, selectWeapon, rand) {
    return weaponObj.draw(direction, x, y, selectWeapon, rand);
  }
}

class Weapon extends GameObject {
  constructor(context, x, y, vx, vy) {
    super(context, x, y, vx, vy);
    this.blaX = 0;
    this.blaY = 0;
    this.direction = "";
    this.weaponAttackID = "";
  }

  draw(direction, x, y, selectWeapon, rand) {
    let weapon = new Image();

    weapon.src = selectWeapon[rand];
    this.blaX = x;
    this.blaY = y;
    this.direction = direction;
    return (weapon.onload = () => {
      this.context.imageSmoothingEnabled = false;
      this.clear(this.blaX, this.blaY, 58, 58);

      if (direction == "right") {
        this.blaX += 20;
      } else if (direction == "left") {
        this.blaX -= 20;
      } else if (direction == "up") {
        this.blaY -= 20;
      } else if (direction == "bottom") {
        this.blaY += 20;
      }
      this.context.drawImage(
        weapon,
        0,
        0,
        320,
        320,
        this.blaX,
        this.blaY,
        220,
        220
      );
      this.weaponAttackID = window.requestAnimationFrame(
        this.draw.bind(
          this,
          this.direction,
          this.blaX,
          this.blaY,
          selectWeapon,
          rand
        )
      );
      if (
        this.blaX > 722 ||
        this.blaX < -22 ||
        this.blaY < -22 ||
        this.blaY > 522
      ) {
        window.cancelAnimationFrame(this.weaponAttackID);
      }
    });
  }
}

class Enemy extends GameObject {
  // Set default width and height
  static width = 50;
  static height = 50;

  constructor(context, x, y, vx, vy) {
    super(context, x, y, vx, vy);
  }

  draw() {
    let enemy = new Image();
    enemy.src = "sprites/beetle_ice.png";

    enemy.onload = () => {
      this.context.imageSmoothingEnabled = false;
      this.clear(this.x - 12, this.y - 12, 68, 68);

      this.context.drawImage(enemy, 32, 0, 40, 40, this.x, this.y, 32, 32); // ????
    };
  }

  update(secondsPassed) {
    // Move with set velocity
    if (!isNaN(secondsPassed)) {
      this.x += this.vx * 3 * secondsPassed;
      this.y += this.vy * 3 * secondsPassed;
    }

    if (this.x > 778) this.x = 0;
    if (this.x < -64) this.x = 778;
    if (this.y > 566) this.y = 0;
    if (this.y < -64) this.y = 566;
  }
}

export { PortfolioGame };
