const knowledgeBase = {
    languages: ["Javascript", "PHP","CSS/SCSS","HTML", "SQL", "all"],
  tools: [
    "Vue",
    "Angular",
    "Laravel",
    "Wordpress",
    "jQuery",
    "Git"
  ],
  languageDescription: [
    {
      javascript: "My primary focus for last three years",
      php: "PHP was always something I worked with",
      css: "fun things",
      html: "bones of every project"
    }
  ],
  toolDescription: [
    {
      vue:
        "Started to use while working on Widewalls rewrite project, at the end of 2019. It's simplicity and reactivity was charming",
      angular:
        "First time I get in touch with Angular was at the end of 2018. Two projects I worked on showed me power of Angular framework. Typescript is probably first benefit when I think of Angular. And there is more to see.",
      scss:
        "The best way for using CSS. It became standard which I try to use as much as I can.",
      laravel:
        "For a long time I had in my mind to start to work with Laravel, as I used PHP for few years now, mostly working on Wordpress platform, or with clean PHP. I fell in love with structure, simplicity and power of Laravel.",
      wordpress:
        "My first jobs were related to Wordpress websites, and I have mixed feelings about it. If you want to build a blog like platform, in a fast and efficient way, Wordpress is a great choice. On the other side, jobs I worked on, were older projects, with dirty and 101% code mess, that was geting worst over the years. So than there is that",
      jQuery:
        "It seems that jQuery becoming more and more thing of the past. Still, I loved to work with it, DOM manipluation, AJAX calls, jQuery was a nice tool to use. And still is, even it is dissapearing more and more",
      git:
        "Not that there is a need to mention it especially, but here it is. Mostly used Github and Gitlab, as git CLIENTS"
    }
  ]
};

export default knowledgeBase;
