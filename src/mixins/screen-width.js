export default {
  data() {
    return {
      isDesktopWidth: true
    };
  },
  methods: {
    isScreenWidthLessThan1024() {
      let screenWidth = window.screen.availWidth;
      if (screenWidth < 1024) {
        this.isDesktopWidth = false;
      }
    }
  }
};
