const myProjects = {
  title: "Projects I worked on",
  projects: [
    {
      title: "My portfolio, version 1",
      image: "portfolio.png",
      keywords: ["Vue", "Vuex", "Javascript", "CSS/SCSS", "Git"],
      duration: "July 2020 - August 2020",
      url: "",
      nickname: "Portfolio",
      git: "https://gitlab.com/k0b1/portfolio-v-01",
      description:
        "<span class='portfolio-u-make-stronger'>Description: </span> It is kind of recursive to add this to my projects list here, but here it is. Built with Vue, idea was to present my previous work, and to have the simplest design, as I am not a designer, not trying to be. Fun part was to copy some of text editor functionalities and outlook, same as to build a little game as the part of the portfolio."
    },
    {
      title: "Noah - integrating Service Now API",
      image: "noah-integrations.png",
      keywords: ["PHP", "API", "jQuery", "Javascript", "Service Now", "Git"],
      duration: "July 2020 - August 2020",
      url: "https://demo.noah-iam.com/login.php",
      nickname: "Noah",
      git: "not available",
      description:
        "<span class='portfolio-u-make-stronger'>Description: </span> My client asked me for the Service Now integration to Noah app, as his new customer needed this integration. My responsibility was to build whole integration, with PHP, jQuery, CSS, and Service Now REST API. I used OOP PHP principles, together with jQuery AJAX calls. Service Now is a huge application, so part of the time I spent on this task was learning of Service Now docs and terminology."
    },
    {
      title: "Widewalls rewrite 2019-2020",
      image: "widewalls-new.png",
      keywords: [
        "Vue",
        "Javascript",
        "CSS/SCSS",
        "Agile/Scrum",
        "team work",
        "Git"
      ],
      duration: "November 2019 - July 2020",
      url: "https://www.widewalls.ch",
      nickname: "Widewalls",
      git: "not available",
      description:
        "<span class='portfolio-u-make-stronger'>Description: </span>After years of fighting with older Wordpress code, decision was made - rewrite whole app and move away from Wordpress legacy code. Chosen technologies were Django plus Vue/NuxtJS. As an inhouse Widewalls developer I worked together with <a class='portfolio-c-my-projects__link no-margin' href='https://www.vivifyideas.com/'>Vivify</a> team from Novi Sad, which took a main role in rebuilding of the app. We worked by Agile/Scrum methodology. My job in project was mostly related to SCSS styling of Vue components, plus occassionaly adding of JS functionalities. In average there were in between 8 to 12 people working on the app, during the 9 months. It was great experience, to work in a team with proper managment."
    },
    {
      title: "Playing cards?",
      image: "cards.png",
      keywords: ["Laravel", "Javascript", "Vue", "CSS/SCSS", "Git"],
      duration: "April 2019 - Jun 2019",
      url: "http://88.198.156.214:8080/",
      nickname: "Playing cards?",
      git: "https://gitlab.com/k0b1/playing-cards",
      description:
        "<span class='portfolio-u-make-stronger'>Description: </span> Build during the Covid-19 outbreak. Friend living in a different city, suggested me - we could try somehow to play 'Tablic', popular card game on territory of ex-Yugoslavia, using shared screens. I thought, well, it would be better to built the game with internet technologies, so we could play it online, over the Internet. Laravel was used for backend, with help of Pusher and Vue components that were used on the front-end. This project will not develop more, it was build for this special situation."
    },
    {
      title: "Nodice - service renting application",
      image: "nodice.png",
      keywords: [
        "Angular",
        "Javascript",
        "PHP",
        "Wordpress",
        "CSS/SCSS",
        "Git"
      ],
      duration: "July 2019 - October 2019",
      url: "https://www.nodice.gnf.dk",
      nickname: "Nodice",
      git: "https://gitlab.com/k0b1/nodice",
      description:
        "<span class='portfolio-u-make-stronger'>Description: </span> Idea for building of this app came from my client, for whom I worked on Noah app. He wanted a prototype, a minimum viable product(MVP) of an app that could be used as a connection between clients who offer/rent their skills (like massage, jewelry, etc) and customers who would buy those skills or products of these skills. My suggestion was to build it with Angular 7 and Wordpress REST API. App included giving of ratings to profiles, STRIPE payment plans, two types of dashboards (for clients and customers), and search functionality, based on business tags, plus, of course, login/sign up module. After building of first, rough version, project went on stand buy. It was a good experience to work with Angular 7 and build single page application with it. Probably in the future, app will be better shaped and more developed."
    },
    {
      title: "Srce, ruke, tastatura - blog, music, programming",
      image: "srt.png",
      keywords: [
        "Angular",
        "Javascript",
        "PHP",
        "Wordpress",
        "CSS/SCSS",
        "Git"
      ],
      duration: "March 2019 - ongoing",
      url: "https://srce-ruke-tastatura.rocks",
      nickname: "Srce, ruke, tastatura",
      git: "https://gitlab.com/k0b1/srce-ruke-tastatura-blog",
      description:
        "<span class='portfolio-u-make-stronger'>Description: </span> This one was simple blog app built with Angular 7+ and Wordpress REST API. It includes section for music, books, blog, and IT releated articles, also search functionalities. It is under development - it develop, as website itself develops."
    },
    {
      title: "Noah - Feeds section",
      image: "noah-feeds.png",
      keywords: ["PHP", "jQuery", "Javascript", "SQL", "MySQL", "Git"],
      duration: "February 2019 - May 2019",
      url: "https://demo.noah-iam.com/login.php",
      nickname: "Noah",
      description:
        "Task was to add Feeds section to Noah app, which will include newest updates of tickets that were made by Noah user, than all tickets related to users company, and simple chat room which will allow related users to have quick communication on subjects they find important at the moment. Built with PHP, jQuery, SQL. ",
      git: "not available"
    },
    {
      title: "Noah - adding of OAT module",
      image: "noah-oat.png",
      keywords: ["PHP", "jQuery", "Javascript", "SQL", "MySQL", "Git"],
      duration: "October 2018 - December 2018",
      url: "https://demo.noah-iam.com/login.php",
      nickname: "Noah",
      description:
        "OAT(Operational Acceptance Test) is a selection of checks to secure an Asset is ready for status change. I had to build whole section of Noah dedicated to this. It included building of a base for OAT questions/tests, relating it to admin section and tickets section of Noah, plus giving reports in PDF format. To do that, I had to have deeper understanding of how different parts of application works, that was probably the most complex thing with this task. Code, by itself, was usual Noah mix of PHP, SQL and jQuery.",
      git: "not available"
    },
    {
      title: "Noah - email reports",
      image: "noah-reporting.png",
      keywords: ["PHP", "jQuery", "Javascript", "SQL", "MySQL", "Git"],
      duration: "February 2018 - May 2018",
      url: "https://demo.noah-iam.com/login.php",
      nickname: "Noah",
      description:
        "My task was to build email reporting system, which will periodically inform Noah customers what changes did happen with their business state - tickets, assets and customers. It was my first bigger task within Noah app, so I had to learn code structure, while creating needed functionality. Cronjob was used for sending of emails, I created HTML email templates also, plus basic logic with PHP and jQuery/vanilla JS.",
      git: "not available"
    },

    {
      title: "Old Widewalls, add messaging system accross the web site",
      image: "widewalls-contact.png",
      keywords: [
        "PHP",
        "Javascript",
        "Wordpress",
        "jQuery",
        "SQL",
        "CSS/SCSS",
        "Git"
      ],
      duration: "Januar 2018 - March 2018",
      url: "https://www.widewalls.ch",
      nickname: "Widewalls",
      git: "not available",
      description:
        "<span class='portfolio-u-make-stronger'>Description: </span> After I started to work for Widewalls website, in 2017, this was the the first bigger task I worked on. Registered Widewalls users can send messages and inquiries to registered art galleries, which sells their art. Galleries, customers, website admins, receives emails, messages, but in fact, it was never clear how this process and logic works. I mentioned it here, as a reminder to myself, how NOT to build things. Older code was a total mess, after years of unorganized changes, coming from  different IT teams that worked on the website. There was no clear description of task. After finishing what I thought that it is my task, I found out that same type of functionality needs to be added on 4 more parts of the web site. I damaged DRY principle, and next two years spent in wanting to fix this. At the end moment never came, because we rebuild the whole web site from the scratch. "
    },
    {
      title: "Atelje Vicic",
      image: "atelje-vicic.png",
      keywords: ["HTML", "CSS/SCSS"],
      duration: "October 2017 - November  2017",
      url: "http://pozdravizbeograda.com/",
      nickname: "Atelje Vicic",
      git: "not available",
      description:
        "<span class='portfolio-u-make-stronger'>Description: </span>The second website I made. It includes only HTML/CSS. Mentioned for historical reasons. :)"
    }
  ]
};

export default myProjects;
